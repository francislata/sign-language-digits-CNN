###
### constants.py
###
### Description:
### This file contains all of the constant values used throughout the project.
###
IMAGE_WIDTH, IMAGE_HEIGHT = 64, 64
INPUT_SHAPE = (IMAGE_WIDTH, IMAGE_HEIGHT)
OUTPUT_COUNT = 10
BATCH_SIZE = 128
EPOCHS = 50
